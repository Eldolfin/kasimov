using UnityEngine;

public abstract class Weapon{
    public abstract Vector3 LeftShoulderRotation { get; }
    public abstract Vector3 LeftArm { get; }
    public abstract Vector3 LeftForeArm { get; }
    public abstract Vector3 LeftHand { get; }
    public abstract Vector3 Neck { get; }
    public abstract Vector3 Neck1 { get; }
    public abstract Vector3 Head { get; }
    public abstract Vector3 RightShoulderRotation { get; }
    public abstract Vector3 RightArm { get; }
    public abstract Vector3 RightForeArm { get; }
    public abstract Vector3 RightHand { get; }

    /*public abstract Vector3 LeftHandIndex1 { get; }
    public abstract Vector3 LeftHandIndex2 { get; }
    public abstract Vector3 LeftHandIndex3 { get; }

    public abstract Vector3 LeftHandMiddle1 { get; }
    public abstract Vector3 LeftHandMiddle2 { get; }
    public abstract Vector3 LeftHandMiddle3 { get; }

    public abstract Vector3 LeftHandPinky1 { get; }
    public abstract Vector3 LeftHandPinky2 { get; }
    public abstract Vector3 LeftHandPinky3 { get; }

    public abstract Vector3 LeftHandRing1 { get; }
    public abstract Vector3 LeftHandRing2 { get; }
    public abstract Vector3 LeftHandRing3 { get; }

    public abstract Vector3 LeftHandThumb1 { get; }
    public abstract Vector3 LeftHandThumb2 { get; }
    public abstract Vector3 LeftHandThumb3 { get; }*/
        
}