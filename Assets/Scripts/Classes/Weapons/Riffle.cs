using UnityEngine;

public class Riffle : Weapon
{
    public override Vector3 LeftShoulderRotation => new Vector3(-2.65f, 26.889f, 2.876f);
    public override Vector3 LeftArm => new Vector3(-58.586f, 27.281f, 40.116f);

    public override Vector3 LeftForeArm => new Vector3(-110.033f, 83.963f, -9.671997f);

    public override Vector3 LeftHand => new Vector3(-26.437f, -3.215f, -7.018f);

    public override Vector3 Neck => new Vector3(-3.302f, 2.13f, -2.881f);

    public override Vector3 Neck1 => new Vector3(-3.302f, 2.13f, -2.881f);

    public override Vector3 Head => new Vector3(-3.302f, 2.13f, -2.881f);
        
    public override Vector3 RightShoulderRotation => new Vector3(-0.08f, 6.713f, 0.134f);

    public override Vector3 RightArm => new Vector3(-9.666f, -43.787f, -42.204f);

    public override Vector3 RightForeArm => new Vector3(-20.015f, -124.868f, 16.259f);
    public override Vector3 RightHand => new Vector3(5.252f, 1.094f, 9.974f);

    /*public override Vector3 LeftHandIndex1 => new Vector3(9.476f, 1.313f, 21.001f);

    public override Vector3 LeftHandIndex2 => new Vector3(0.707f,-2.79f, 28.433f);

    public override Vector3 LeftHandIndex3 => new Vector3(0.711f,-2.798f, 28.526f);

    public override Vector3 LeftHandMiddle1 => new Vector3(10.052f,1.834f, 20.965f);

    public override Vector3 LeftHandMiddle2 => new Vector3(1.765f,-4.192f, 45.563f);

    public override Vector3 LeftHandMiddle3 => new Vector3(1.625f,-4.049f, 43.595f);

    public override Vector3 LeftHandPinky1 => new Vector3(-7.935f,-1.623f,6.72f);
    public override Vector3 LeftHandPinky2 => new Vector3(2.198f,-4.575f,51.3f);
    public override Vector3 LeftHandPinky3 => new Vector3(0.88f,-3.089f,31.811f);

    public override Vector3 LeftHandRing1 => new Vector3(1.254f,0.078f,17.947f);
    public override Vector3 LeftHandRing2 => new Vector3(3.728f,-5.461f,68.61f);
    public override Vector3 LeftHandRing3 => new Vector3(5.221f,-9.671f,32.861f);

    public override Vector3 LeftHandThumb1 => new Vector3(5.221f,-9.671f,32.861f);
    public override Vector3 LeftHandThumb2 => new Vector3(0.684f,-9.498f,2.3f);
    public override Vector3 LeftHandThumb3 => new Vector3(5.831f,-12.714f,-2.659f);*/


}