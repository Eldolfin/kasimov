// using UnityEngine;
// using System;
// using UnityEngine.UI;
//
// public class Commandes : MonoBehaviour
// {
//
//     private GameObject[] gameManager;
//
//     private GameObject gameObj;
//
//     private InputController controller;
//     public Text text;
//     public int index;
//     bool x = false;
//
//     private void Awake()
//     {
//         gameManager = GameObject.FindGameObjectsWithTag("GameManager");            //on cherche les objets avec le tag "GameManager"
//         gameObj = gameManager[0];                                                  //on selectionne le premier de ces objets
//         controller = gameObj.GetComponent<InputController>();         // on r�cupere le script InputController dedans
//         ChangeText();
//     }
//     public void ChangeInputKey()
//     {
//         x = true;
//     }
//
//     public void ChangeText()
//     {
//         text.text = controller.GetList(index).ToString();         //on change le texte pour mettre le nom de la touche attribu�e
//     }
//
//     private void Update()
//     {
//           if (x)                      //si on veut changer l'input
//             {
//                 foreach (KeyCode elt in Enum.GetValues(typeof(KeyCode)))      //on va regarder tous les KeyCode possibles
//                 {
//                     if (Input.GetKey(elt))                                    //on regarde si l'utilisateur touche le clavier et que la touche est une touche existante
//                     {
//                         controller.ModifTouchKeys(elt, index);                //on change la valeur de la touche dans l'InputController 
//                         x = false;                                            //on ne veut plus changer la touche
//                         ChangeText();                                         //on update le texte
//                     }
//
//                 }
//             }
//     }
//
// }
