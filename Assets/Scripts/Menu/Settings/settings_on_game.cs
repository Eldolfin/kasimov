using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

public class settings_on_game : MonoBehaviour
{
    public GameObject gameObj;
    public GameObject gameObj2;
    public bool menu = false;

    // Start is called before the first frame update
    void Start()
    {
        menu = false;
    }

    public void Curseur()
    {
        if (!menu)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    public void Get_input()
    {
        if (Keyboard.current.escapeKey.wasPressedThisFrame)
        {
            if (menu)
            {
                resume();
            }
            else
            {
                paused();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        Curseur();
        Get_input();
    }

    public void paused()
    {
        menu = true;
        gameObj.SetActive(true);
        gameObj2.SetActive(false);
    }

    public void resume()
    {
        menu = false;
        gameObj.SetActive(false);
        gameObj2.SetActive(true);
    }
}