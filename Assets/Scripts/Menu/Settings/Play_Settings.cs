using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Play_Settings : MonoBehaviour
{
    private NetworkManager NManager;
    public GameObject Play;
    public GameObject Join;
    // Start is called before the first frame update
    void Start()
    {
        NManager = NetworkManager.singleton;
    }
    
    public void ChangeJoin()
    {
        Play.SetActive(!Play.activeSelf);
        Join.SetActive(!Join.activeSelf);
    }

    public void Host()
    {
        NManager.StartHost();
    }

    public void Client()
    {
        NManager.StartClient();
    }
}
