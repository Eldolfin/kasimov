using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Mirror;



public class Script_settings : MonoBehaviour
{

    [SerializeField] private bool fullscreen;
    //mofifs test antoine
    private GameObject[] gameManager;

    // private GameObject gameObj;

    private NetworkManagerHUD NetManager;

    Resolution[] resolution;
    public Dropdown ResDropdown;

    public GameObject global;
    public GameObject commandes;
    public GameObject autres;


    private void Awake()
    {
        NetManager = GameObject.FindWithTag("NetworkManager").GetComponent<NetworkManagerHUD>();
    }

    // Start is called before the first frame update
    void Start()
    {
        resolution = Screen.resolutions.Select(resolutions => new Resolution { width = resolutions.width, height = resolutions.height }).Distinct().ToArray();
        ResDropdown.ClearOptions();             //on nettoie le dropdown

        List<string> option = new List<string>();

        int currentres = 0;

        for (int i = 0; i < resolution.Length; i++)            //on cherche les résolutions acceptées par l'ordinateur
        {
            string opt = resolution[i].width + "x" + resolution[i].height;
            option.Add(opt);
            if (resolution[i].width == Screen.width && resolution[i].height == Screen.height)
            {
                currentres = i;
            }
        }

        ResDropdown.AddOptions(option);                     //on ajoute les résolutions trouvées au dropdown
        ResDropdown.value = currentres;
        ResDropdown.RefreshShownValue();                    //on refresh le dropdown
    }

    public void SetResolution(int ResIndex)
    {
        Resolution resol = resolution[ResIndex];
        Debug.Log("resoluion changed Scrips/Menu/Settings/Script_settings.cs/SetResolution()");
        Screen.SetResolution(resol.width, resol.height, fullscreen);     //on change la résolution de l'écran par celle demandée
    }
    public void Global()
    {
        global.SetActive(true);
        commandes.SetActive(false);
        autres.SetActive(false);
        // gameObj.SetActive(false);
    }
    public void Commandes()
    {
        global.SetActive(false);
        commandes.SetActive(true);
        autres.SetActive(false);
        // gameObj.SetActive(false);
    }
    public void Autres()
    {
        global.SetActive(false);
        commandes.SetActive(false);
        autres.SetActive(true);
        // gameObj.SetActive(false);
    }
    public void Autres_online()
    {
        global.SetActive(false);
        commandes.SetActive(false);
        autres.SetActive(true);
        // gameObj.SetActive(true);
    }

    public void Out()
    {
        NetManager.StopButtons();
    }
}
