using UnityEngine;
using UnityEngine.UI;


public class Script_menu : MonoBehaviour
{
    // Start is called before the first frame update

    public Text text;
    public GameObject perso;
    public GameObject play;
    public GameObject profil;
    public GameObject settings;
    public GameObject messages;
    public GameObject friends;
    public GameObject Play_Panel;
    public GameObject Join_Panel;
    public Play_Settings playSet;

    public GameObject[] games;
    // Start is called before the first frame update

    private void Start()
    {
        perso.SetActive(true);
        // play.SetActive(false);
        games=new [] {profil,settings,messages,friends};
    }

    public void ChangePlay()
    {

        if (Join_Panel.activeSelf)
        {
            Join_Panel.SetActive(false);
        }
        else
        {
            Play_Panel.SetActive(!Play_Panel.activeSelf);
        }
    }
    
    
    public void Play()
    {
        // Active(play);
        if (Join_Panel.activeSelf || Play_Panel.activeSelf)
        {
            text.text = "Play";
        }
        else
        {
            text.text = "Annuler";
        }

    }
    public void Active(GameObject activ)
    {
        foreach (GameObject elt in games)
        {
            if (elt!=activ)
            {
                elt.SetActive(false);
            }

        }
        Join_Panel.SetActive(false);
        Play_Panel.SetActive(false);
        text.text = "Play";
        activ.SetActive(!activ.activeSelf);

    }

    public void Quit_Obj(GameObject elt)
    {
        elt.SetActive(false);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
