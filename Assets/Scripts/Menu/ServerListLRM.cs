using System;
using System.Collections;
using System.Collections.Generic;
using LightReflectiveMirror;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ServerListLRM : MonoBehaviour
{
    public Transform scrollParent;
    public GameObject serverEntry;

    private LightReflectiveMirrorTransport _lrm;

    private void Start()
    {
        if (_lrm == null)
        {
            _lrm = (LightReflectiveMirrorTransport) Transport.activeTransport;
            _lrm.serverListUpdated.AddListener(UpdateServerList);
        }
    }

    public void RefreshServerList()
    {
        _lrm.RequestServerList();
    }
    void UpdateServerList()
    {
        foreach (Transform t in scrollParent)
        {
            Destroy(t);
        }

        for (int i = 0; i < _lrm.relayServerList.Count; i++)
        {
            var entry = Instantiate(serverEntry, scrollParent);
            entry.transform.GetChild(0).GetComponent<Text>().text = _lrm.relayServerList[i].serverName;

            var serverId = _lrm.relayServerList[i].serverId;
            entry.GetComponent<Button>().onClick.AddListener(() => ConnectServer(serverId));
        }
    }

    void ConnectServer(string serverId)
    {
        NetworkManager.singleton.networkAddress = serverId;
        NetworkManager.singleton.StartClient();
    }

    private void OnDisable()
    {
        if (_lrm != null)
        {
            _lrm.serverListUpdated.RemoveListener(UpdateServerList);
        }
    }
}