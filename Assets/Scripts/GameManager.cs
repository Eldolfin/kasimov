using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private const string playerIdPrefix = "Player";

    private static Dictionary<string, Player> players = new Dictionary<string, Player>();

    public GameSettings gameSettings;

    public static GameManager singleton;

    public PlayerInputActions _inputActions;


    private void Awake()
    {
        if (singleton != null)
        {
            // si on crée un nouveau gamemanager, on vire l'autre
            Destroy(singleton);
        }

        _inputActions = new PlayerInputActions();

        // Pour l'instant on limite la les fps au refresh rate de l'ecran mais c'est sensé devenir un reglage
        Application.targetFrameRate = Screen.currentResolution.refreshRate;

        DontDestroyOnLoad(this);

        singleton = this;
    }

    public static void RegisterPlayer(string netID, Player player)
    {
        string playerId = playerIdPrefix + netID;
        players.Add(playerId, player);
        player.transform.name = playerId;
    }

    public static void UnregisterPlayer(string playerId)
    {
        players.Remove(playerId);
    }

    public static Player GetPlayer(string playerId)
    {
        return players[playerId];
    }

    private void OnEnable()
    {
        _inputActions.Enable();
    }

    private void OnDisable()
    {
        if (!(_inputActions is null))
        {
            _inputActions.Disable();
        }
    }
}