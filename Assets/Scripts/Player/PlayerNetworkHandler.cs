using Mirror;
using UnityEngine;

public class PlayerNetworkHandler : NetworkBehaviour
{
    [SerializeField] private Behaviour[] localOnlyBehaviours;

    [SerializeField] private string remoteLayerName = "RemotePlayer";
    [SerializeField] private string localLayerName = "LocalPlayer";

    private void Start()
    {
        if (!isLocalPlayer)
        {
            DisableLocalComponents();
            Utils.SetLayerRecursively(gameObject, LayerMask.NameToLayer(remoteLayerName));
        }
        else
        {
            Utils.SetLayerRecursively(gameObject, LayerMask.NameToLayer(localLayerName));
        }
        
        GetComponent<Player>().Setup();
    }

    private void DisableLocalComponents()
    {
        foreach (Behaviour behaviour in localOnlyBehaviours)
        {
            behaviour.enabled = false;
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        string netId = GetComponent<NetworkIdentity>().netId.ToString();
        Player player = GetComponent<Player>();

        GameManager.RegisterPlayer(netId, player);
    }
}