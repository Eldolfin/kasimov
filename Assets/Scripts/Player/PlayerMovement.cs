using System;
using UnityEngine;
using UnityEngine.InputSystem;

//Script for moving a gameObject smoothly
//Usage: Attach the character controller component to the gameobject that you want to move
public class PlayerMovement : MonoBehaviour
{
    // place the gameobject that you want to move to the controller placeholder
    public CharacterController controller;

    public float speed = 5f;
    public float gravity = 9.81f;
    public float jumpSpeed = 4f;
    public float mouseSensitivity = 0.2f;
    public float crouchSpeedModifier = 0.5f;
    public float sprintSpeedModifier = 2f;
    

    [HideInInspector] public Vector2 movementInput;
    private float _verticalSpeed;
    private Transform playerTransform;

    private PlayerInputActions _inputActions;

    private bool canDoubleJump;

    public bool Sprinting;

    public bool Crouching;

    private bool canLook = true;

    public bool Jumping
    {
        get => !controller.isGrounded && _verticalSpeed > 0;
    }

    private void Start()
    {
        _inputActions = GameManager.singleton._inputActions;
        playerTransform = transform;
        canLook = false;
        // // cette ligne ajoute un listener: la valeur movementInput est update toute seule en continue
        _inputActions.Player.Move.performed += ctx => movementInput = ctx.ReadValue<Vector2>();
        _inputActions.Player.Move.canceled += ctx => movementInput = Vector2.zero;
        _inputActions.Player.Jump.performed += Jump;
        _inputActions.Player.Look.performed += LookUpdate;
        _inputActions.Player.Crouch.performed += context => Crouching = true;
        _inputActions.Player.Crouch.canceled += context => Crouching = false;
        _inputActions.Player.Sprint.performed += context => Sprinting = true;
        _inputActions.Player.Sprint.canceled += context => Sprinting = false;
    }

    void FixedUpdate()
    {
        
        if (!controller.isGrounded)
            _verticalSpeed -= gravity * Time.deltaTime;
        Vector3 acceleration;
        if (!canLook)
        {
            acceleration = movementInput.x * GetSpeed() * playerTransform.right +
                               _verticalSpeed * playerTransform.up +
                               movementInput.y * GetSpeed() * playerTransform.forward;
        }
        else
        {
            acceleration = _verticalSpeed * playerTransform.up;
        }
        controller.Move(acceleration * Time.deltaTime);


    }

    private void Update()
    {
        if (Keyboard.current.escapeKey.wasPressedThisFrame)
        {
            canLook = !canLook;
        }
    }

    public void close()
    {
        canLook = !canLook;
    }

    private float GetSpeed()
    {
        if (Crouching)
            return speed * crouchSpeedModifier;
        if (Sprinting)
            return speed * sprintSpeedModifier;
        return speed;
    }

    private void Jump(InputAction.CallbackContext ctx)
    {
        if (!canLook)
        {
            if (controller.isGrounded)
            {
                _verticalSpeed = jumpSpeed;
                canDoubleJump = true;
            }
            else if (canDoubleJump)
            {
                _verticalSpeed += jumpSpeed;
                canDoubleJump = false;
            }
        }
    }

    private void LookUpdate(InputAction.CallbackContext ctx)
    {
        if (!canLook)
        {
            Vector2 mouseInput = ctx.ReadValue<Vector2>();
            transform.Rotate(0, mouseInput.x * mouseSensitivity, 0);
        }
    }

    private void OnDisable()
    {
        
            _inputActions.Player.Move.performed -= ctx => movementInput = ctx.ReadValue<Vector2>();
            _inputActions.Player.Move.canceled -= ctx => movementInput = Vector2.zero;
            _inputActions.Player.Jump.performed -= Jump;
            _inputActions.Player.Look.performed -= LookUpdate;
            _inputActions.Player.Crouch.performed -= context => Crouching = true;
            _inputActions.Player.Crouch.canceled -= context => Crouching = false;
            _inputActions.Player.Sprint.performed -= context => Sprinting = true;
            _inputActions.Player.Sprint.canceled -= context => Sprinting = false;
        
    }
}