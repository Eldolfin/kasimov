using UnityEngine;

public class PlayerPositionInitializer : MonoBehaviour
{
    #region Player Body Parts

// ici poiur les objets
    private Transform LeftShoulder;
    private Transform LeftArm;
    private Transform LeftForeArm;
    private Transform LeftHand;
    private Transform Neck;
    private Transform Neck1;
    private Transform Head;
    private Transform RightShoulder;
    private Transform RightArm;
    private Transform RightForeArm;
    private Transform RightHand;

    // ajouter les doigt cv faire mal
/*
    private Transform LeftHandIndex1;
    private Transform LeftHandIndex2;
    private Transform LeftHandIndex3;

    private Transform LeftHandMiddle1;
    private Transform LeftHandMiddle2;
    private Transform LeftHandMiddle3;


    private Transform LeftHandPinky1;
    private Transform LeftHandPinky2;
    private Transform LeftHandPinky3;

    private Transform LeftHandRing1;
    private Transform LeftHandRing2;
    private Transform LeftHandRing3;

    private Transform LeftHandThumb1;
    private Transform LeftHandThumb2;
    private Transform LeftHandThumb3;*/


/////////////////////////////////

    #endregion

    void Start()
    {
        // foreach (var obj in GetComponent<PlayerCamera>().following_head)
        // {
        //     var netTransform = gameObject.AddComponent<NetworkTransformChild>();
        //     netTransform.clientAuthority = true;
        //     netTransform.target = obj.transform;
        // }
        InitializeBodyParts();
        SwapWeapon(new Riffle());
    }


    private void SwapWeapon(Weapon weapon)
    {
        // swat:LeftShoulder
        LeftShoulder.Rotate(weapon.LeftShoulderRotation);

        // swat:LeftArm
        LeftArm.Rotate(weapon.LeftArm);

        // swat:LeftForeArm
        LeftForeArm.Rotate(weapon.LeftForeArm);

        // swat:LeftHand
        LeftHand.Rotate(weapon.LeftHand);

        // swat:Neck
        Neck.Rotate(weapon.Neck);

        // swat:Neck1
        Neck1.Rotate(weapon.Neck1);

        // swat:Head
        Head.Rotate(weapon.Head);

        // swat:RightShoulder
        RightShoulder.Rotate(weapon.RightShoulderRotation);

        // swat:RightArm
        RightArm.Rotate(weapon.RightArm);

        // swat:RightForeArm
        RightForeArm.Rotate(weapon.RightForeArm);

        // swat:RightHand
        RightHand.Rotate(weapon.RightHand);
/*
        // swat:LeftHandIndex1
        LeftHandIndex1.Rotate(weapon.LeftHandIndex1);

        // swat:LeftHandIndex2
        LeftHandIndex2.Rotate(weapon.LeftHandIndex2);

        // swat:LeftHandIndex3
        LeftHandIndex3.Rotate(weapon.LeftHandIndex3);

        // swat:LeftHandMiddle1
        LeftHandMiddle1.Rotate(weapon.LeftHandMiddle1);

        // swat:LeftHandMiddle2
        LeftHandMiddle2.Rotate(weapon.LeftHandMiddle2);

        // swat:LeftHandMiddle3
        LeftHandMiddle3.Rotate(weapon.LeftHandMiddle3);

        // swat:LeftHandPinky1
        LeftHandPinky1.Rotate(weapon.LeftHandPinky1);

        // swat:LeftHandPinky2
        LeftHandPinky2.Rotate(weapon.LeftHandPinky2);

        // swat:LeftHandPinky3
        LeftHandPinky3.Rotate(weapon.LeftHandPinky3);

        // swat:LeftHandRing1
        LeftHandRing1.Rotate(weapon.LeftHandRing1);

        // swat:LeftHandRing2
        LeftHandRing2.Rotate(weapon.LeftHandRing2);

        // swat:LeftHandRing3
        LeftHandRing3.Rotate(weapon.LeftHandRing3);

        // swat:LeftHandThumb1
        LeftHandThumb1.Rotate(weapon.LeftHandThumb1);

        // swat:LeftHandThumb2
        LeftHandThumb2.Rotate(weapon.LeftHandThumb2);

        // swat:LeftHandThumb3
        LeftHandThumb3.Rotate(weapon.LeftHandThumb3);*/
    }
    
    private void InitializeBodyParts()
    {
        foreach (Transform g in transform.Find("Swat").Find("swat:Hips").GetComponentsInChildren<Transform>())
        {
            switch (g.name)
            {
                case "swat:LeftShoulder":
                    LeftShoulder = g;
                    break;
                case "swat:LeftArm":
                    LeftArm = g;
                    break;
                case "swat:LeftForeArm":
                    LeftForeArm = g;
                    break;
                case "swat:LeftHand":
                    LeftHand = g;
                    break;
                case "swat:Neck":
                    Neck = g;
                    break;
                case "swat:Neck1":
                    Neck1 = g;
                    break;
                case "swat:Head":
                    Head = g;
                    break;
                case "swat:RightShoulder":
                    RightShoulder = g;
                    break;
                case "swat:RightArm":
                    RightArm = g;
                    break;
                case "swat:RightForeArm":
                    RightForeArm = g;
                    break;
                case "swat:RightHand":
                    RightHand = g;
                    break;
                /*
                case "swat:LeftHandIndex1":
                    LeftHandIndex1 = g;
                    break;
                case "swat:LeftHandIndex2":
                    LeftHandIndex2 = g;
                    break;
                case "swat:LeftHandIndex3":
                    LeftHandIndex3 = g;
                    break;
                case "swat:LeftHandMiddle1":
                    LeftHandMiddle1 = g;
                    break;
                case "swat:LeftHandMiddle2":
                    LeftHandMiddle2 = g;
                    break;
                case "swat:LeftHandMiddle3":
                    LeftHandMiddle3 = g;
                    break;
                case "swat:LeftHandPinky1":
                    LeftHandPinky1 = g;
                    break;
                case "swat:LeftHandPinky2":
                    LeftHandPinky2 = g;
                    break;
                case "swat:LeftHandPinky3":
                    LeftHandPinky3 = g;
                    break;
                case "swat:LeftHandRing1":
                    LeftHandPinky1 = g;
                    break;
                case "swat:LeftHandRing2":
                    LeftHandPinky2 = g;
                    break;
                case "swat:LeftHandRing3":
                    LeftHandPinky3 = g;
                    break;
                case "swat:LeftHandThumb1":
                    LeftHandPinky1 = g;
                    break;
                case "swat:LeftHandThumb2":
                    LeftHandPinky2 = g;
                    break;
                case "swat:LeftHandThumb3":
                    LeftHandPinky3 = g;
                    break;*/
            }        
        }
    }
}