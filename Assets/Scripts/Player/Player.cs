using System.Collections;
using Mirror;
using UnityEngine;

public class Player : NetworkBehaviour
{
    [SyncVar] private bool _isDead;

    public bool isDead
    {
        get { return _isDead; }
        protected set { _isDead = value; }
    }

    [SerializeField] private float maxHealth = 100f;

    [SyncVar] private float currentHealth;

    [SerializeField] private Component[] disableOnDeath;
    private bool[] wasEnabledOnStart;

    private HealthPlayer healthBar;

    public void Setup()
    {
        if (isLocalPlayer)
        {
            healthBar = GameObject.FindWithTag("HealthBar").GetComponent<HealthPlayer>();
        }

        wasEnabledOnStart = new bool[disableOnDeath.Length];
        for (int i = 0; i < disableOnDeath.Length; i++)
        {
            if (disableOnDeath[i] is Renderer)
            {
                wasEnabledOnStart[i] = ((Renderer) disableOnDeath[i]).enabled;
            }
            else if (disableOnDeath[i] is Behaviour)
            {
                wasEnabledOnStart[i] = ((Behaviour) disableOnDeath[i]).enabled;
            }
        }

        SetDefaults();
    }

    public void SetDefaults()
    {
        isDead = false;
        currentHealth = maxHealth;

        for (int i = 0; i < disableOnDeath.Length; i++)
        {
            if (disableOnDeath[i] is Renderer)
            {
                ((Renderer) disableOnDeath[i]).enabled = wasEnabledOnStart[i];
            }
            else if (disableOnDeath[i] is Behaviour)
            {
                ((Behaviour) disableOnDeath[i]).enabled = wasEnabledOnStart[i];
            }
        }

        Collider col = GetComponent<Collider>();
        if (col != null)
        {
            col.enabled = true;
        }
        
        if(isLocalPlayer)
            healthBar.SetHealth(currentHealth);
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(GameManager.singleton.gameSettings.respawnDelay);
        SetDefaults();
        Transform spawnPoint = NetworkManager.singleton.GetStartPosition();
        transform.position = spawnPoint.position;
        transform.rotation = spawnPoint.rotation;
    }

    [ClientRpc]
    public void RpcTakeDamage(float amount, string shooterName)
    {
        if (isDead)
        {
            return;
        }

        currentHealth -= amount;
        if (isLocalPlayer)
            healthBar.SetHealth(currentHealth);
        Debug.Log($"{shooterName} a enlevé {amount} pts de vie à {name}");
        if (currentHealth <= 0)
        {
            Die(shooterName);
        }
    }

    private void Die(string shooterName)
    {
        isDead = true;

        Debug.Log(shooterName + " killed " + name);

        for (int i = 0; i < disableOnDeath.Length; i++)
        {
            if (disableOnDeath[i] is Renderer)
            {
                ((Renderer) disableOnDeath[i]).enabled = false;
            }
            else if (disableOnDeath[i] is Behaviour)
            {
                ((Behaviour) disableOnDeath[i]).enabled = false;
            }
        }

        Collider col = GetComponent<Collider>();
        if (col != null)
        {
            col.enabled = false;
        }


        StartCoroutine(Respawn());
    }
}