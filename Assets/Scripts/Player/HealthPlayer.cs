using UnityEngine;
using UnityEngine.UI;

public class HealthPlayer : MonoBehaviour
{
    //script qui est responsable de l'affichage de la barre de vie

    public Slider slide;

    public Gradient gradient;

    public Image fill;

    public void SetMaxHealth(float health)
    {
        slide.maxValue = health;
        slide.value = health;

        fill.color = gradient.Evaluate(1f);
    }

    public void SetHealth(float health)
    {
        slide.value = health;

        fill.color = gradient.Evaluate(slide.normalizedValue);
    }
}