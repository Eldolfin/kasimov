using System;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerMovement))]
public class PlayerCamera : MonoBehaviour
{
    public float camRotationLimit = 60;

    Camera _camera;
    private float camRotationX;
    private float mouseSensitivity;
    private PlayerInputActions _inputActions;

    [SerializeField]
    public GameObject[] following_head;

    private bool canLook;

    private void Start()
    {
        canLook = false;
        _camera = GetComponentInChildren<Camera>();
        mouseSensitivity = GetComponent<PlayerMovement>().mouseSensitivity;
        _inputActions = GameManager.singleton._inputActions;
        _inputActions.Player.Look.performed += LookUpdate;
    }

    private void LookUpdate(InputAction.CallbackContext ctx)
    {
        if (!canLook)
        {
            Vector2 mouseInput = ctx.ReadValue<Vector2>();
            camRotationX -= mouseInput.y * mouseSensitivity;
            camRotationX = Mathf.Clamp(camRotationX, -camRotationLimit, camRotationLimit);
            _camera.transform.localEulerAngles = camRotationX * Vector3.right;
            RotateFollowingObjects();
        }
    }

    private void RotateFollowingObjects(){
        foreach (var obj in following_head)
        {
            obj.transform.localEulerAngles = camRotationX * Vector3.right;
        }  
    }

    private void OnDisable()
    {
        _inputActions.Player.Look.performed -= LookUpdate;
    }
    private void Update()
    {
        if (Keyboard.current.escapeKey.wasPressedThisFrame)
        {
            canLook = !canLook;
        }
    }

    public void close()
    {
        canLook = !canLook;
    }
}