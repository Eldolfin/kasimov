﻿// using System;
// using Mirror;
// using UnityEngine;
//
// public class Bullet : NetworkBehaviour
// {
//     [SerializeField] public float speed; //speed to ball
//     [SerializeField] float damage; //impact of character
//     [SerializeField] float livetime; // // bool //si rencontre un mur meur ou si dépasse une certaine hauteur elle meurt
//     [SerializeField] private float raycastRange = 1000;
//     [SerializeField] private LayerMask hittableMask;
//
//     [HideInInspector] public Player shooter;
//
//     [HideInInspector] public Vector3 velocity = Vector3.zero;
//
//
//     // ajouter d'autre parametre peut être
//     //trouver des idées
//
//     private void Start()
//     {
//         // GetComponent<NetworkIdentity>()
//         //     .AssignClientAuthority(shooter.GetComponent<NetworkIdentity>().connectionToClient);
//         transform.SetParent(null);
//         Destroy(gameObject, livetime); //détruire object après un certain tant
//         velocity += Vector3.forward * speed * Time.deltaTime;
//     }
//
//     [Server]
//     private void Update()
//     {
//         transform.Translate(velocity);
//
//         RaycastHit hit;
//
//         if (Physics.Raycast(transform.position, transform.forward, out hit, raycastRange, hittableMask))
//         {
//             Debug.Log("touché (Bullets.cs/update)");
//             if (hit.collider.CompareTag("Player"))
//             {
//                 // CmdPlayerShot(, damage, shooter.name);
//                 GameManager.GetPlayer(hit.collider.name).RpcTakeDamage(damage, shooter.name);
//             }
//
//             Destroy(gameObject);
//         }
//     }
//
//     [Command]
//     private void CmdPlayerShot(string victimName, float damage, string shooterName)
//     {
//         Debug.Log(victimName + " touché ;)");
//     }
// }