using UnityEngine;

// Shooter est un behaviour pour les weapons

public class Shooter : MonoBehaviour
{
    [SerializeField]public float firetime; // La cadance de tire que l'on peux changer par rapport a chaque arms
    [SerializeField] public float damage=10;
    public bool isAutomatic;
    float nextFireAllowed; //prochain tire
    public bool isReloading;
    public float range = 1000;
    
    public void FireEffect()
    {
        // ICI POUR LES BRUITS D'ARMES
    }
}
