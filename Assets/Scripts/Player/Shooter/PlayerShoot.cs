using Mirror;
using UnityEngine;

public class PlayerShoot : NetworkBehaviour
{
    [SerializeField] Shooter weapon; //choose of weapon in this
    [SerializeField] private LayerMask mask;
    [SerializeField] private Camera PlayerCam;

    PlayerInputActions _playerInputActions;

    private void Start()
    {
        if (!isLocalPlayer)
            return;
        _playerInputActions = GameManager.singleton._inputActions;
        _playerInputActions.Player.Fire.performed += ctx => StartShooting();
        _playerInputActions.Player.Fire.canceled += ctx => CancelInvoke("Shoot");
    }

    void StartShooting()
    {
        if (weapon.isAutomatic)
            InvokeRepeating("Shoot", 0, weapon.firetime);
        else
            Shoot();
    }
    

    [Client]
    void Shoot()
    {
        if (!isLocalPlayer || weapon.isReloading)
            return;

        RaycastHit _hit;
        bool didHit = Physics.Raycast(PlayerCam.transform.position, PlayerCam.transform.forward, out _hit, weapon.range,
            mask);
        if (didHit)
        {
            if (_hit.collider.CompareTag("Player"))
            {
                CmdPlayerShot(_hit.collider.name, weapon.damage, transform.name);
            }
        }
        CmdShoot(_hit.point,didHit);
    }

    [Command]
    void CmdShoot(Vector3 hitpos,bool didHit)
    {
        RpcShoot(hitpos,didHit);
    }

    [ClientRpc]
    void RpcShoot(Vector3 hitpos,bool didHit)
    {
        weapon.FireEffect();
        if (didHit)
            Debug.DrawLine(PlayerCam.transform.position, hitpos, Color.green, 3,false);
        else
            Debug.DrawRay(PlayerCam.transform.position, PlayerCam.transform.forward*1000, Color.red, 1,false);
    }

    [Command]
    void CmdPlayerShot(string victimID, float damage, string shooterID)
    {
        GameManager.GetPlayer(victimID).RpcTakeDamage(damage, shooterID);
    }
}