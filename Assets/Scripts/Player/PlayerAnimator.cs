using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    Animator animator;
    private PlayerMovement _playerMovement;

    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        _playerMovement = GetComponent<PlayerMovement>();
    }

    private void Update()
    {
        animator.SetFloat("Vertical", _playerMovement.movementInput.y);
        animator.SetFloat("Horizontal", _playerMovement.movementInput.x);
        animator.SetBool("Jump",_playerMovement.Jumping);
        animator.SetBool("Sprinting", _playerMovement.Sprinting);
        animator.SetBool("Crounching", _playerMovement.Crouching);
    }
}